name             'edu-ui'
maintainer       'NIBR'
maintainer_email 'josef.ringgenberg@novartis.com'
license          'All rights reserved'
description      'Installs/Configures edu-ui'
version          '0.2.3'

depends          'anyapp', '>= 0.3.5'
depends         "ci"
depends         "nginx"